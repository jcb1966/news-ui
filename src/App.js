import React, { memo } from "react";
import { HashRouter, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
// import { renderRoutes } from "react-router-config";

import ShellFrame from "./components/shell-frame";
import Login from "./pages/login";

import store from "./store";

export default memo(function App() {
  return (
    <Provider className="app" store={store}>
      <HashRouter>
        <Switch>
          <Route path="/login" component={Login}></Route>
          <Route path="/" render={()=>
                    localStorage.getItem("token")?
                    <ShellFrame ></ShellFrame>:
                    <Redirect to="/login"/>
                }/>
        </Switch>
        {/* {localStorage.getItem("token") ? (
          <ShellFrame></ShellFrame>
        ) : (
            <Redirect to="/login"></Redirect>
        )} */}
      </HashRouter>
    </Provider>
  );
});
