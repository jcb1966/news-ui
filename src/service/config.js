const devBaseUrl = "http://localhost:8000/";
const proBaseUrl = "http://localhost:8000/";

export const BASE_URL =
  process.env.NODE_ENV === "development" ? devBaseUrl : proBaseUrl;

export const TIME_OUT = 5000;
