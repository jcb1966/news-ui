import request from "./request"

export const requestPublishManageList=(username,type)=>{
  return request({
    url:`/news`,
    params:{
      author:username,
      publishState:type,
      _expand:"category"
    }
  })
}