import request from "./request"


// 请求侧边栏的数据
export const requestMenuList=()=>{
  return request({
    url:"/rights?_embed=children"
  })
}

export const requestRight=()=>{
  return request({
    url:"/rights"
  })
}

export const requestChildren=()=>{
  return request({
    url:"/children"
  })
}