import request from "./request"

export const userLogin=(params)=>{
  return request({
    url:"/users",
    method:"get",
    params:{
      ...params,
      roleState:true,
      _expand:"role"
    }
  })
}