import request from "./request";

// 请求权限列表的相关数据
export const requestRightList = () => {
  return request({
    url: "/rights?_embed=children",
  });
};
// 删除一级权限
export const requestDelRight = (id) => {
  return request({
    url: `/rights/${id}`,
    method: "delete",
  });
};
// 删除二级权限
export const requestDelRightOfTwo = (id) => {
  return request({
    url: `/children/${id}`,
    method: "delete",
  });
};
// 修改权限的状态
export const requestEditRightPagepermisson = (id, pagepermisson, path) => {
  return request({
    url: `/${path}/${id}`,
    method: "patch",
    data: {
      pagepermisson,
    },
  });
};
// 请求角色列表的数据
export const requestRoleList = () => {
  return request({
    url: "/roles",
  });
};
// 删除某一个角色
export const requestDelRole=(id)=>{
  return request({
    url:`/roles/${id}`,
    method:"delete"
  })
}
// 跟新角色列表中，角色所具备的权限
export const requestUpdateRoleRights=(id,roleRightList)=>{
  return request({
    url:`/roles/${id}`,
    method:"patch",
    data:{
      rights:roleRightList
    }
  })
}
