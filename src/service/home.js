import request from "./request"

export const requestViewList=()=>{
  return request({
    url:"/news?publishState=2&_expand=category&_sort=view&_order=desc&_limit=6"
  })
}

export const requestStarList=()=>{
  return request({
    url:"/news?publishState=2&_expand=category&_sort=star&_order=desc&_limit=6"
  })
}