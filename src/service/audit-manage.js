import request from "./request";
export const requestAuditList = (username) => {
  return request({
    url: "/news",
    params: {
      author: username,
      auditState_ne: 0,
      publishState_lte: 1,
      _expand: "category",
    },
  });
};

export const requestAuditListOfUnpublish=()=>{
  return request({
    url:"/news",
    params:{
      auditState:1,
      _expand: "category",
    }
  })
}
