import request from "./request";

export const requestUserList = () => {
  return request({
    url: "/users?_expand=role",
  });
};

export const requestRegionList = () => {
  return request({
    url: "/regions",
  });
};

export const requestAddUser = (data) => {
  return request({
    url: "/users",
    method: "post",
    data: data,
  });
};

export const requestDeleteUser = (id) => {
  return request({
    url: `/users/${id}`,
    method: "delete",
  });
};

export const requestUpdateUserState = (id, roleState) => {
  request({
    url: `/users/${id}`,
    method: "patch",
    data: {
      roleState,
    },
  });
};

export const requestUpdateUser = (id, users) => {
  return request({
    url: "/users/" + id,
    method: "patch",
    data:users
  });
};
