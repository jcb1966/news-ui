import request from "./request";

export const requestNewsCategory = () => {
  return request({
    url: "/categories",
  });
};

export const requestAddNews = (data) => {
  return request({
    url: "/news",
    method: "post",
    data: data,
  });
};

export const requestNewstList = (username, auditState) => {
  return request({
    url: "/news",
    params: {
      author: username,
      auditState: auditState,
      _expand: "category",
    },
  });
};

export const requestDeleteNews = (id) => {
  return request({
    url: "/news/" + id,
    method: "delete",
  });
};

export const requestCurrentNews = (id) => {
  return request({
    url: "/news/" + id + "?_expand=category&_expand=role",
    // params:{
    //   _expand:"category&_expand=role",
    // }
  });
};

export const updateNews = (id, data) => {
  return request({
    url: `/news/${id}`,
    method: "patch",
    data: data,
  });
};

export const deleteCategory=(id)=>{
  return request({
    url:`/categories/${id}`,
    method:"delete"
  })
}

export const updateCategory=(id,data)=>{
  return request({
    url:`/categories/${id}`,
    method:"patch",
    data,
  })
}
