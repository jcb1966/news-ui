
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  // 反向代理
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'https://i.maoyan.com',
      changeOrigin: true,
    })
  );
};