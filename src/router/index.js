import { lazy } from "react";
import { Redirect } from "react-router-dom";

const Home = lazy(() => import("../pages/home"));
// const Login = lazy(() => import("../pages/login"));
// const ShellFrame=lazy(()=>import("../components/shell-frame"))
const UserList = lazy(() => import("../pages/user-manage/user-list"));
const RightList = lazy(() => import("../pages/right-manage/right-list"));
const RoleList = lazy(()=>import("../pages/right-manage/role-list"))

const routes = [
  {
    path: "/",
    exact: true,
    render() {
      return <Redirect to="/home"></Redirect>;
    },
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/user-manage/list",
    component: UserList,
  },
  {
    path: "/right-manage/right/list",
    component: RightList,
  },
  {
    path:"/right-manage/role/list",
    component:RoleList
  }
];

export default routes;
