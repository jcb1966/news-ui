import { combineReducers } from "redux-immutable";
import { shellFrameReducer } from "../components/shell-frame/store/";
import { rightManageReducer } from "../pages/right-manage/store";
import { userListReducer } from "../pages/user-manage/store";
import { userReducer } from "../pages/login/store";
import { newsManageReducer } from "../pages/news-manage/store";
import { auditManageReducer } from "../pages/audit-manage/store";
import { publishManageReducer } from "../pages/publish-manage/store";
const reducer = combineReducers({
  shellFrame: shellFrameReducer,
  rightManage: rightManageReducer,
  userManage: userListReducer,
  user: userReducer,
  newsManage: newsManageReducer,
  auditManage: auditManageReducer,
  publishManage:publishManageReducer
});

export default reducer;
