import { message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

const usePublish = (type, action) => {
  const [publishListState, setPublishListState] = useState([]);

  const { publishList } = useSelector((state) => {
    return {
      publishList: state.getIn(["publishManage", "publishList"]),
    };
  }, shallowEqual);

  const dispatch = useDispatch();
  useEffect(() => {
    const { username } = JSON.parse(localStorage.getItem("token"));
    dispatch(action(username, type));
  }, [dispatch, action, type]);
  useEffect(() => {
    setPublishListState(publishList);
  }, [publishList]);

  const btnClick = (id, requestMethod, params) => {
    // console.log(id);
    setPublishListState(publishListState.filter((item) => item.id !== id));
    params && requestMethod(id, params) && message.success("操作成功!");
    !params && requestMethod(id) && message.success("删除成功!");
  };

  return {
    publishListState,
    btnClick,
  };
};

export default usePublish;
