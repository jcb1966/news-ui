import React, { memo, Suspense } from "react";
// import { renderRoutes } from "react-router-config";
// import { Redirect } from "react-router-dom";

import { Layout } from "antd";

import SideMenu from "./side-menu";
import TopHeader from "./top-header";
import { ShellFrameWrapper } from "./style";
import Router from "./router";

// import routes from "../../router";

const { Content } = Layout;
export default memo(function ShellFrame(props) {
  return (
    <ShellFrameWrapper>
      <Layout>
        {/* 侧边栏 */}
        <SideMenu></SideMenu>
        <Layout className="site-layout">
          {/* 头部导航 */}
          <TopHeader></TopHeader>
          {/* 内容主题 */}
          <Content className="site-layout-background index-content">
            <Suspense
              fallback={
                null
              }
            >
              {/* {localStorage.getItem("token") ? renderRoutes(routes) : <> </>} */}
              {/* {renderRoutes(routes)} */}
              <Router></Router>
            </Suspense>
          </Content>
        </Layout>
      </Layout>
    </ShellFrameWrapper>
  );
});
