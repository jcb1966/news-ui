import React, { memo, useEffect, useCallback, useState, useMemo } from "react";
import { withRouter } from "react-router-dom";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import Nprogress from "nprogress";
import "nprogress/nprogress.css";

import { Layout, Menu } from "antd";

import { SideMenuWrapper } from "../style";

import { getMenuListAction } from "../store/action";

const { Sider } = Layout;
const { SubMenu } = Menu;

const SideMenu = memo(function (props) {
  // 默认打开的一级菜单的路径,刷新之后默认打开的路径
  const defaultOpenKeys = "/" + props.location.pathname.split("/")[1];
  // console.log(defaultOpenKeys);
  /* state */
  const [openKeys, setOpenKeys] = useState([defaultOpenKeys]); //当前打开的父级菜单
  // console.log(openKeys);
  /* redux */
  // 获取侧边栏数据
  const { menuList } = useSelector((state) => {
    return {
      menuList: state.getIn(["shellFrame", "menuList"]),
    };
  }, shallowEqual);

  const dispatch = useDispatch();

  /* hook */
  // 注意：不能在useEffect的第一个回调函数上使用async/await
  // 派发侧边栏数据相关的acting
  useEffect(() => {
    dispatch(getMenuListAction());
  }, [dispatch]);
  
  useEffect(() => {
    Nprogress.done();
  });
  // 获取所有父级菜单的Key路径
  const rootSubmenuKeys = useMemo(() => {
    const arr = [];
    menuList.forEach((item) => {
      arr.push(item.key);
    });
    return arr;
  }, [menuList]);

  // 事件调用
  // 点击菜单，切换页面
  const changePage = useCallback(
    (item) => {
      Nprogress.start();
      props.history.push(item.key);
    },
    [props.history]
  );

  // 切换父级菜单，关闭其他的父级菜单
  const onOpenChange = useCallback(
    (keys) => {
      const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1);
      if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
        setOpenKeys(keys);
      } else {
        setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
      }
    },
    [openKeys, rootSubmenuKeys]
  );

  /* 其他相关的业务逻辑 */
  // 用于渲染左侧菜单栏的函数
  const userInfo = JSON.parse(localStorage.getItem("token"));
  const renderMenuList = (menuList) => {
    return menuList.map((item) => {
      // pagepermisson=1的数据才需要展示在侧边栏，不是等于1的数据要展示在页面内部
      if (
        item.children &&
        item.children.length > 0 &&
        item.pagepermisson === 1 &&
        userInfo.role.rights.includes(item.key)
      ) {
        return (
          <SubMenu key={item.key} icon={item.icon} title={item.title}>
            {renderMenuList(item.children)}
          </SubMenu>
        );
      }
      return (
        item.pagepermisson === 1 &&
        userInfo.role.rights.includes(item.key) && (
          <Menu.Item
            key={item.key}
            icon={item.icon}
            onClick={(e) => changePage(item)}
          >
            {item.title}
          </Menu.Item>
        )
      );
    });
  };

  return (
    <SideMenuWrapper>
      <Sider trigger={null} collapsible>
        <div className="sideMenuDiv">
          <div className="logo">全球新闻发布管理系统</div>
          <div className="menuOpen">
            <Menu
              theme="dark"
              mode="inline"
              defaultSelectedKeys={[props.location.pathname]}
              selectedKeys={[props.location.pathname]}
              defaultOpenKeys={[defaultOpenKeys]}
              onOpenChange={onOpenChange}
              openKeys={openKeys}
            >
              {renderMenuList(menuList)}
            </Menu>
          </div>
        </div>
      </Sider>
    </SideMenuWrapper>
  );
});
// 由于当前组件不是直接由react-router渲染的，所以不能直接使用props.history.push
// 这路需要使用withRouter高阶组件才行
export default withRouter(SideMenu);
