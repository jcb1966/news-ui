import { Map } from "immutable";

import * as actionType from "./constants";

const defaultShellFrameState = Map({
  menuList: [],
  routerList: [],
});

function shellFrameReducer(state = defaultShellFrameState, action) {
  switch (action.type) {
    case actionType.CHANGE_MENU_LIST:
      return state.set("menuList", action.menuList);
    case actionType.CHANGE_ROUTER_LIST:
      return state.set("routerList", action.routerList);
    default:
      return state;
  }
}

export default shellFrameReducer;
