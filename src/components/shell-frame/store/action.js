import {
  requestMenuList,
  requestRight,
  requestChildren,
} from "@/service/shell-frame";
import * as actionType from "./constants";

/* 内部自己使用的action */
// 获取侧边栏列表
const changeMenuListAction = (res) => {
  return {
    type: actionType.CHANGE_MENU_LIST,
    menuList: res,
  };
};

const changeRouterAction=(res)=>{
  // console.log(res);
  return {
    type:actionType.CHANGE_ROUTER_LIST,
    routerList:res
  }
}

/* 向外暴露的action */
// 获取侧边栏列表
export const getMenuListAction = () => {
  return (dispatch) => {
    requestMenuList().then((res) => {
      dispatch(changeMenuListAction(res));
    });
  };
};

export const getRouterAction = () => {
  return (dispatch) => {
    Promise.all([requestRight(), requestChildren()]).then((res) => {
      // console.log(res);
      dispatch(changeRouterAction([...res[0],...res[1]]))
    });
  };
};
