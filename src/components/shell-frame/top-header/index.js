import React, { memo, useState, useCallback } from "react";
import { withRouter } from "react-router-dom";

import { Layout, Avatar, Dropdown, Menu, Modal } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { TopHeaderWrapper } from "../style";

const { Header } = Layout;
const { confirm } = Modal;

const TopHeader = memo(function (props) {
  /* state */
  const [collapsed, setCollapsed] = useState(false); //用于控制侧边栏的显示与折叠

  const userInfo = JSON.parse(localStorage.getItem("token"));
  /* hook */
  const changeOutlined = useCallback(() => {
    setCollapsed(!collapsed);
  }, [collapsed]);

  const backLogin = useCallback(() => {
    confirm({
      title: "提示",
      icon: <ExclamationCircleOutlined />,
      content: "你确定退出当前账号吗?",
      okText: "确定",
      cancelText: "取消",
      onOk() {
        localStorage.removeItem("token");
        props.history.push("/login");
      },
      onCancel() {},
    });
  }, [props.history]);

  /* 其他业务逻辑 */
  const menu = (
    <Menu>
      <Menu.Item key="role">{userInfo.role.roleName}</Menu.Item>
      <Menu.Item
        danger
        key="back"
        onClick={(e) => {
          backLogin();
        }}
      >
        退出
      </Menu.Item>
    </Menu>
  );

  return (
    <TopHeaderWrapper>
      <Header className="site-layout-background header-padding">
        {/* 左侧菜单折叠按钮 */}
        {collapsed ? (
          <MenuUnfoldOutlined className="trigger" onClick={changeOutlined} />
        ) : (
          <MenuFoldOutlined className="trigger" onClick={changeOutlined} />
        )}
        {/* 右侧用户信息 */}
        <div className="header-user-msg">
          <span>{userInfo.username}</span>
          <Dropdown overlay={menu} arrow>
            <Avatar
              className="header-user-avatar"
              size="large"
              icon={<UserOutlined />}
            />
          </Dropdown>
        </div>
      </Header>
    </TopHeaderWrapper>
  );
});

export default withRouter(TopHeader);
