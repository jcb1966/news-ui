import React, { memo, lazy, useEffect, useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Result } from "antd";

import { getRouterAction } from "../store/action";

const Home = lazy(() => import("@/pages/home"));
const UserList = lazy(() => import("@/pages/user-manage/user-list"));
const RightList = lazy(() => import("@/pages/right-manage/right-list"));
const RoleList = lazy(() => import("@/pages/right-manage/role-list"));
const AddNews = lazy(() => import("@/pages/news-manage/add-news"));
const Draft = lazy(() => import("@/pages/news-manage/draft"));
const NewsPreview=lazy(()=>import("@/pages/news-manage/news-preview"))
const NewsUpdate=lazy(()=>import("@/pages/news-manage/news-update"))
const Category = lazy(() => import("@/pages/news-manage/category"));
const AuditNews = lazy(() => import("@/pages/audit-manage/audit-news"));
const AuditList = lazy(() => import("@/pages/audit-manage/audit-list"));
const Unpublished = lazy(() => import("@/pages/publish-manage/unpublished"));
const Published = lazy(() => import("@/pages/publish-manage/published"));
const Sunset = lazy(() => import("@/pages/publish-manage/sunset"));

export default memo(function Router() {
  const [router, setRouter] = useState([]);
  const userInfo = JSON.parse(localStorage.getItem("token"));
  // console.log(userInfo);

  const { routerList } = useSelector((state) => {
    return {
      routerList: state.getIn(["shellFrame", "routerList"]),
    };
  }, shallowEqual);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getRouterAction());
  }, [dispatch]);
  useEffect(() => {
    setRouter(routerList);
  }, [routerList]);
  // console.log(routerList);
  // 判断某个页面的权限是否打开
  const pageIsShowAboutAll = (item) => {
    // pagepermisson=0，不展示在侧边栏
    return (
      LocalRouterMap[item.key] && (item.pagepermisson || item.routepermisson)
    );
  };
  // 判断当前用户是否具备打开某个页面的权限
  const pageIsShowAboutUser = (item) => {
    return userInfo.role.rights.includes(item.key);
  };

  const LocalRouterMap = {
    "/home": Home,
    // 用户管理
    "/user-manage/list": UserList,
    // 权限管理
    "/right-manage/role/list": RoleList,
    "/right-manage/right/list": RightList,
    // 新闻管理
    "/news-manage/add": AddNews,
    "/news-manage/draft": Draft,
    "/news-manage/category": Category,
    "/news-manage/preview/:id": NewsPreview,
    "/news-manage/update/:id": NewsUpdate,
    // 审核管理
    "/audit-manage/audit": AuditNews,
    "/audit-manage/list": AuditList,
    // 发布管理
    "/publish-manage/unpublished": Unpublished,
    "/publish-manage/published": Published,
    "/publish-manage/sunset": Sunset,
  };
  return (
    <div>
      <Switch>
        {router.map((item) => {
          if (pageIsShowAboutAll(item) && pageIsShowAboutUser(item)) {
            return (
              <Route
                exact
                path={item.key}
                component={LocalRouterMap[item.key]}
                key={item.id}
              ></Route>
            );
          }
          return (
            <Route
              exact
              path={item.key}
              render={() => {
                return (
                  <div>
                    <Result
                      status="403"
                      title="403"
                      subTitle="你没有权限访问该网页"
                    />
                  </div>
                );
              }}
              key={item.id}
            ></Route>
          );
        })}
        {/* <Route exact path="/home" component={Home}></Route>
        <Route path="/user-manage/list" component={UserList}></Route>
        <Route path="/right-manage/role/list" component={RoleList}></Route>
        <Route path="/right-manage/right/list" component={RightList}></Route> */}
        <Redirect exact from="/" to="/home"></Redirect>
        {router.length > 0 && (
          <Route
            path="*"
            render={() => {
              return (
                <div>
                  <Result status="404" title="404" subTitle="页面找不到了" />
                </div>
              );
            }}
          ></Route>
        )}
      </Switch>
    </div>
  );
});
