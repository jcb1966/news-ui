import styled from "styled-components";

export const ShellFrameWrapper = styled.div`
  #components-layout-demo-custom-trigger .trigger {
    padding: 0 24px;
    font-size: 18px;
    line-height: 64px;
    cursor: pointer;
    transition: color 0.3s;
  }

  #components-layout-demo-custom-trigger .trigger:hover {
    color: #1890ff;
  }

  #components-layout-demo-custom-trigger {
    height: 32px;
    margin: 16px;
    background: rgba(255, 255, 255, 0.3);
  }

  .index-content {
    margin: 24px 16px;
    padding: 24px;
    minheight: 280px;
    overflow: auto;
  }
  .site-layout .site-layout-background {
    background: #fff;
  }
`;

export const SideMenuWrapper = styled.div`
  background-color: #001529;
  .ant-layout-sider {
    height: 100%;
  }
  .logo {
    height: 32px;
    margin: 16px 5px;
    background: rgba(255, 255, 255, 0.3);
    font-size: 18px;
    color: white;
    text-align: center;
    line-height: 32px;
  }
  .sideMenuDiv {
    display: flex;
    height: 100%;
    flex-direction: column;
    .menuOpen {
      flex: 1;
      overflow: auto;
    }
  }
`;

export const TopHeaderWrapper = styled.div`
  .trigger {
    margin: 0 16px;
  }
  .header-user-msg {
    float: right;
  }
  .header-user-avatar {
    margin: 0 16px;
  }
  .header-padding {
    padding: 0;
  }
`;

export const LoadingWrapper = styled.div`
  margin: auto;
  margin-bottom: 20px;
  padding: 30px 50px;
  text-align: center;
  background: rgba(0, 0, 0, 0.05);
  border-radius: 4px;
`;
