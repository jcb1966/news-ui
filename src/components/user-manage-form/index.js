import React, {
  memo,
  forwardRef,
  useState,
  useCallback,
  useEffect,
} from "react";

import { Form, Input, Select } from "antd";

const { Option } = Select;

const UserManageForm = memo(
  forwardRef((props, ref) => {
    // console.log(ref);
    /* state */
    //区域选择框是否被禁用
    const [regionSelectIsDisabled, setRegionSelectIsDisabled] = useState(false);
    const userInfo = JSON.parse(localStorage.getItem("token"));

    useEffect(() => {
      setRegionSelectIsDisabled(props.regionFormItemIdDisabled);
    }, [props.regionFormItemIdDisabled]);

    /* 事件调用 */
    // 监听角色选择框的变化
    const changeRole = useCallback(
      (value) => {
        // console.log(value);
        // 选择的角色是超级管理员
        if (value === 1) {
          // 禁用区域选择框
          setRegionSelectIsDisabled(true);
          // 将区域选择框中的值设置为空
          ref.current.setFieldsValue({
            region: "",
          });
        } else {
          // 不是超级管理员
          setRegionSelectIsDisabled(false);
        }
      },
      [ref]
    );

    // 表单验证规则
    const suernameFormRules = [
      {
        required: true,
        min: 3,
        max: 12,
        message: "请输入3-12个字符之间的用户名",
      },
    ];
    const passwordFormRules = [
      {
        required: true,
        min: 6,
        max: 18,
        message: "请输入6-18个字符的密码!",
      },
    ];
    const regionFormRules = regionSelectIsDisabled
      ? []
      : [
          {
            required: true,
            message: "请选择区域!",
          },
        ];
    const roleFormRules = [
      {
        required: true,
        message: "请选择角色!",
      },
    ];
    return (
      <div>
        <Form
          layout="horizontal"
          ref={ref}
          autoComplete="off"
          validateTrigger="onBlur"
        >
          <Form.Item name="username" label="用户名" rules={suernameFormRules}>
            <Input />
          </Form.Item>
          <Form.Item name="password" label="密码" rules={passwordFormRules}>
            <Input.Password />
          </Form.Item>
          <Form.Item name="region" label="区域" rules={regionFormRules}>
            <Select disabled={regionSelectIsDisabled}>
              {props.regionList.map((item) => {
                return (
                  <Option
                    value={item.value}
                    key={item.id}
                    disabled={userInfo.roleId === 1 ? false : userInfo.region !== item.value}
                  >
                    {item.title}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <Form.Item name="roleId" label="角色" rules={roleFormRules}>
            <Select onChange={(value) => changeRole(value)}>
              {props.roleList.map((item) => {
                return (
                  <Option
                    value={item.id}
                    key={item.id}
                    disabled={
                      userInfo.roleId === 1
                        ? false
                        :( item.id === 3
                        ? false
                        : true)
                    }
                  >
                    {item.roleName}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
        </Form>
      </div>
    );
  })
);

export default UserManageForm;
