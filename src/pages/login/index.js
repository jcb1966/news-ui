import React, { memo, useCallback } from "react";
import { useDispatch } from "react-redux";

import { Form, Input, Button, message } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

import { LoginWrapper } from "./style";

import { userLogin } from "../../service/login";
import { saveUserInfoAction } from "./store/action";

export default memo(function Login(props) {
  const dispatch = useDispatch();

  const onFinish = useCallback(
    (values) => {
      userLogin(values).then((res) => {
        if (res.length) {
          dispatch(saveUserInfoAction(res[0]));
          localStorage.setItem("token", JSON.stringify(res[0]));
          props.history.push("/");
        } else {
          message.error("用户名或密码错误！");
        }
      });
    },
    [props.history, dispatch]
  );
  return (
    <LoginWrapper>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <div className="login_title">全球新闻发布管理系统</div>
        <Form.Item
          name="username"
          rules={[{ required: true, message: "请输入用户名!" }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="用户名"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "请输入密码!" }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="密码"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            登录
          </Button>
        </Form.Item>
      </Form>
    </LoginWrapper>
  );
});
