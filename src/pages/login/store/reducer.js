
import { Map } from "immutable";

import * as actionType from "./constants";

const defaultUserInfoState = Map({
  userInfo: null,
});

function userReducer(state = defaultUserInfoState, action) {
  switch (action.type) {
    case actionType.CHANGE_USER_MSG:
      return state.set("userInfo", action.userInfo);
    default:
      return state;
  }
}

export default userReducer;
