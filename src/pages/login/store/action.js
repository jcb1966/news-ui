import * as actionType from "./constants"

export const saveUserInfoAction=(userInfo)=>{
  return {
    type:actionType.CHANGE_USER_MSG,
    userInfo
  }
}