import styled from "styled-components";

import bgImg from "../../assets/img/bg.jpg"

export const LoginWrapper = styled.div`
  /* background-color: rgb(35, 39, 65); */
  background-image:url(${bgImg});
  height: 100%;
  background-size: cover;
  background-position: center;
  /* overflow: hidden; */
  .login-form {
    position: fixed;
    background-color: rgba(0, 0, 0, 0.7);
    width: 500px;
    height: 300px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    padding: 20px;
    /* z-index: 100; */
    .login_title {
      text-align: center;
      height: 80px;
      line-height: 80px;
      font-size: 30px;
      color: white;
    }
    .login-form-button{
      position:absolute;
      left:50%;
      transform: translate(-50%, -50%);
    }
  }
`;
