import { requestRightList,requestRoleList } from "@/service/right-manage"
import * as actionType from "./constants"


/* 内部自己使用的action */
// 修改权限列表
const changeRightListAction=(res)=>{
  return {
    type:actionType.CHANGE_RIGHT_LIST,
    rightList:res
  }
}
// 修改角色列表
const changeRoleListAction=(res)=>{
  return {
    type:actionType.CHANGE_ROLE_LIST,
    roleList:res
  }
}



/* 向外暴露的action */
// 获取权限列表
export const getRightListAction=()=>{
  return dispatch=>{
    requestRightList().then(res=>{
      dispatch(changeRightListAction(res))
    })
  }
}
// 获取角色列表
export const getRoleListAction=()=>{
  return dispatch=>{
    requestRoleList().then(res=>{
      dispatch(changeRoleListAction(res))
    })
  }
}