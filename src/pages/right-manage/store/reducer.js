import * as actionType from "./constants";
import { Map } from "immutable";

const defaultRightListState = Map({
  rightList: [],
  roleList:[]
});

function rightManageReducer(state = defaultRightListState, action) {
  switch (action.type) {
    case actionType.CHANGE_RIGHT_LIST:
      return state.set("rightList", action.rightList);
    case actionType.CHANGE_ROLE_LIST:
      return state.set("roleList",action.roleList)
    default:
      return state;
  }
}

export default rightManageReducer
