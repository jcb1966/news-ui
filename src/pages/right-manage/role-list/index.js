import React, { memo, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

import { Table, Button, Modal, Tree, message } from "antd";
import {
  DeleteOutlined,
  AlignCenterOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { getRoleListAction } from "../store/action";
import {
  requestDelRole,
  requestUpdateRoleRights,
} from "@/service/right-manage";

const { confirm } = Modal;

export default memo(function RoleList() {
  /* state */
  const [roleListState, setRoleListState] = useState([]); //用于保存角色列表的数据
  const [isModalVisible, setIsModalVisible] = useState(false); //记录对话框的关闭/打开
  // 记录点击修改权按钮后，所点击的那一行角色所具备的所有权限列表
  const [currentClickRoleRightsList, setCurrentClickRoleRightsList] = useState(
    []
  );
  const [currentRoleId, setCurrentRoleId] = useState(null);

  /* redux */
  // 从redux中获取角色列表的数据
  const { roleList, menuList } = useSelector((state) => {
    return {
      roleList: state.getIn(["rightManage", "roleList"]), //角色列表
      menuList: state.getIn(["shellFrame", "menuList"]), //所有的权限-子权限列表
    };
  }, shallowEqual);
  const dispatch = useDispatch();
  /* hook */
  // 保存角色列表的数据
  useEffect(() => {
    setRoleListState(roleList);
  }, [roleList]);
  useEffect(() => {
    dispatch(getRoleListAction());
  }, [dispatch]);

  /* 事件调用 */
  // 监听删除角色按钮的点击
  const deleteRoleBtnClick = useCallback(
    (item) => {
      // console.log(item);
      confirm({
        title: "你确定要删除此角色吗?",
        icon: <ExclamationCircleOutlined />,
        okText: "确定",
        okType: "danger",
        cancelText: "返回",
        onOk() {
          // console.log(item);
          setRoleListState(roleListState.filter((data) => data.id !== item.id));
          requestDelRole(item.id);
          message.success("删除成功！", 1);
        },
        onCancel() {
          message.success("已撤销删除！", 1);
        },
      });
    },
    [roleListState]
  );

  // 监听对话框中“确定”按钮的点击，并关闭对话框
  const handleOk = useCallback(() => {
    // 关闭对话框
    // console.log(currentClickRoleRightsList);
    const newRoleListState = roleListState.map((item) => {
      if (item.id === currentRoleId) {
        return {
          ...item,
          rights: currentClickRoleRightsList,
        };
      }
      return item;
    });
    /* 
    为什么要重新设置一下roleListState?  刷新对话框中复选框中的值
    点击修改权限按钮，会调用openModal(item),在这个函数中，我们给currentClickRoleRightsList从新赋值
    了，这就依赖了roleListState的值，如果不给roleListState赋值，则从新代开对话框时，界面上展示的还是
    原来选中的哪些数据
    */
    setRoleListState(newRoleListState);
    // 发送网络请求，修改后端数据
    requestUpdateRoleRights(currentRoleId, currentClickRoleRightsList);
    setIsModalVisible(false);
    message.success("权限修改成功！", 1);
  }, [currentRoleId, currentClickRoleRightsList, roleListState]);
  // 监听对话框的关闭
  const handleCancel = useCallback(() => {
    // 关闭对话框
    setIsModalVisible(false);
    message.success("已取消修改权限", 1);
  }, []);
  // 监听修改权限按钮的点击，打开对话框
  const openModal = useCallback((item) => {
    // console.log(item);
    // 保存当前点击的这一行角色所具备的权限列表
    setCurrentClickRoleRightsList(item.rights);
    // console.log(item.id);
    // 保存当前点击角色的id
    setCurrentRoleId(item.id);
    // 打开对话框
    setIsModalVisible(true);
  }, []);
  // 监听对话框中复选框的点击
  const checkClick = useCallback((checkedKeys) => {
    // checkedKeys是当前角色修改之后的权限列表
    // console.log(checkedKeys.checked);
    // 保存修改后的角色所具备的权限列表
    setCurrentClickRoleRightsList(checkedKeys.checked);
    // console.log(currentClickRoleRightsList);
  }, []);

  /* 其他业务逻辑 */
  // 表格所需的数据结构
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      render(id) {
        return <b>{id}</b>;
      },
    },
    {
      title: "角色名称",
      dataIndex: "roleName",
    },
    {
      title: "操作",
      render(item) {
        return (
          <>
            <Button
              danger
              type="danger"
              shape="circle"
              icon={<DeleteOutlined />}
              className="button-icon-marging"
              onClick={(e) => deleteRoleBtnClick(item)}
            />
            <Button
              type="primary"
              shape="circle"
              icon={<AlignCenterOutlined />}
              onClick={(e) => openModal(item)}
            />
          </>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        dataSource={roleListState}
        columns={columns}
        pagination={{
          pageSize: 5,
        }}
        rowKey={(item) => item.id}
      ></Table>
      <Modal
        title="修改权限"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText="取消"
        okText="确定"
      >
        <Tree
          checkable
          checkStrictly
          treeData={menuList}
          checkedKeys={currentClickRoleRightsList}
          onCheck={(checkedKeys) => checkClick(checkedKeys)}
        />
      </Modal>
    </div>
  );
});
