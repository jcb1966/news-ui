import React, { memo, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";

import { Table, Tag, Button, Modal, Popover, Switch, message } from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { getRightListAction } from "../store/action";
import {
  requestDelRight,
  requestDelRightOfTwo,
  requestEditRightPagepermisson,
} from "@/service/right-manage";

const { confirm } = Modal;

export default memo(function RightList() {
  /* state */
  const [rightListState, setRightListState] = useState([]); //用于保存权限列表的数据
  /* redux */
  const { rightList } = useSelector((state) => {
    return {
      rightList: state.getIn(["rightManage", "rightList"]),
    };
  }, shallowEqual);
  const dispatch = useDispatch();

  /* hook */
  useEffect(() => {
    dispatch(getRightListAction());
  }, [dispatch]);
  // 将权限列表的数据保存在state hook中，以便于后面的操作
  useEffect(() => {
    setRightListState(rightList);
  }, [rightList]);

  /* 事件调用 */
  // 监听删除权限按钮的点击
  const deleteRightClick = useCallback(
    (item) => {
      // console.log("删除项", item);
      confirm({
        title: "你确定要删除此权限吗?",
        icon: <ExclamationCircleOutlined />,
        okText: "确定",
        okType: "danger",
        cancelText: "返回",
        onOk() {
          // 删除一级权限(二级权限跟着删除)
          if (item.grade === 1) {
            setRightListState(
              // 过滤掉需要删除的权限
              rightListState.filter((data) => data.id !== item.id)
            );
            requestDelRight(item.id);
          } else {
            // 删除二级权限
            // 找到所删除的二级权限的父级权限
            let rightFatherList = rightListState.filter(
              (data) => data.id === item.rightId
            );
            // console.log(rightFatherList[0].children);
            rightFatherList[0].children = rightFatherList[0].children.filter(
              (data) => data.id !== item.id
            );
            // console.log(rightListState);
            // rightListState是引用数据类型，不能这样写
            // setRightListState(rightListState)
            // 为什么rightListState改变了？
            // filter()函数返回数组的第一层是改变了的，但是更深层次的数据还是引用以前的数据(这里的数据是引用数据类型)
            // 所以地址还是以前的，我们进行上一步的赋值操作的时候改变了旧地址中的值，所以rightListState跟着改变了，
            // 所以有时候修改原数据并非一件坏事
            setRightListState([...rightListState]);
            requestDelRightOfTwo(item.id);
          }
          message.success("删除成功！", 1);
        },
        onCancel() {
          // console.log("Cancel");
          message.success("已撤销删除！", 1);
        },
      });
    },
    [rightListState]
  );

  // 监听配置项开关改变
  const switchBtnClick = useCallback((item) => {
    // 修改pagepermisson的值，从而去控制用户是否可见侧边栏对应的项
    item.pagepermisson = item.pagepermisson === 1 ? 0 : 1;
    // console.log(rightListState);
    setRightListState([...rightListState]);
    if (item.grade === 1) {
      requestEditRightPagepermisson(item.id, item.pagepermisson, "rights");
      // axios.path("")
    } else {
      requestEditRightPagepermisson(item.id, item.pagepermisson, "children");
    }
    message.success("修改成功！", 1);
  },[rightListState]);

  /* 其他业务逻辑 */
  // 首页的权限列表的数据存在空的数组children，影响列表的展开，
  // 所以这里把首页的children修改成字符串
  if (rightList[0] && !rightList[0].children.length) {
    rightList[0].children = "";
  }
  // 权限列表所需的数据结构
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      render(id) {
        return <b>{id}</b>;
      },
    },
    {
      title: "权限名称",
      dataIndex: "title",
    },
    {
      title: "权限路径",
      dataIndex: "key",
      render(key) {
        return <Tag color="orange">{key}</Tag>;
      },
    },
    {
      title: "操作",
      render(item) {
        return (
          <>
            <Button
              danger
              type="danger"
              shape="circle"
              icon={<DeleteOutlined />}
              className="button-icon-marging"
              onClick={(e) => deleteRightClick(item)}
            />
            <Popover
              content={
                <div className="x-center">
                  <Switch
                    checked={item.pagepermisson}
                    onChange={(e) => switchBtnClick(item)}
                  />
                </div>
              }
              title="配置项"
              trigger={item.pagepermisson === undefined ? "" : "click"}
            >
              <Button
                /* 
                  pagepermisson代表是否展示在侧边栏
                  0：不展示  1：展示
                  通过控制pagepermisson的值，可以控制用户是否能看到侧边栏的某些选项
                  无论pagepermisson值是多少，只要存在pagepermisson这一项，“编辑”按钮就应当可以点击
                  如果数据中不存在pagepermisson，则说明这个数据是展示在页面中的，“编辑”按钮不能够点击
                  所以不能使用 !item.pagepermisson 去控制disabled的值
                */
                disabled={item.pagepermisson === undefined}
                type="primary"
                shape="circle"
                icon={<EditOutlined />}
              />
            </Popover>
          </>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        dataSource={rightListState}
        columns={columns}
        pagination={{
          pageSize: 5,
        }}
      />
    </div>
  );
});
