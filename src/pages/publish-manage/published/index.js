import React, { memo } from "react";
import { Button } from "antd";

import { getPublishManageListAction } from "../store/action";
import PublishTable from "@/components/publish-table/PublishTable";
import usePublish from "@/components/publish-table/usePublish";
import { updateNews } from "@/service/news-manage";

export default memo(function Published() {
  const { publishListState, btnClick } = usePublish(
    2,
    getPublishManageListAction
  );

  return (
    <div>
      <PublishTable
        dataSource={publishListState}
        button={(id) => (
          <Button
            type="primary"
            onClick={() =>
              btnClick(id, updateNews, {
                publishState: 3,
              })
            }
          >
            下线
          </Button>
        )}
      ></PublishTable>
    </div>
  );
});
