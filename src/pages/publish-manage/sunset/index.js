import React, { memo } from "react";
import { Button } from "antd";

import { getPublishManageListAction } from "../store/action";
import PublishTable from "@/components/publish-table/PublishTable";
import usePublish from "@/components/publish-table/usePublish";
import { requestDeleteNews } from "@/service/news-manage";

export default memo(function Sunset() {
  const { publishListState, btnClick } = usePublish(
    3,
    getPublishManageListAction
  );

  return (
    <div>
      <PublishTable
        dataSource={publishListState}
        button={(id) => (
          <Button
            type="primary"
            onClick={() => btnClick(id, requestDeleteNews)}
          >
            删除
          </Button>
        )}
      ></PublishTable>
    </div>
  );
});
