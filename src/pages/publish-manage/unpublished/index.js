import React, { memo } from "react";
import { Button } from "antd";

import { getPublishManageListAction } from "../store/action";
import PublishTable from "@/components/publish-table/PublishTable";
import usePublish from "@/components/publish-table/usePublish";
import { updateNews } from "@/service/news-manage";

export default memo(function Unpublished() {
  const { publishListState, btnClick } = usePublish(
    1,
    getPublishManageListAction
  );


  return (
    <div>
      <PublishTable
        dataSource={publishListState}
        button={(id) => (
          <Button
            type="primary"
            onClick={() =>
              btnClick(id, updateNews, {
                publishState: 2,
                publishTime: Date.now(),
              })
            }
          >
            发布
          </Button>
        )}
      ></PublishTable>
    </div>
  );
});
