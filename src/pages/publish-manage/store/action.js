import { requestPublishManageList } from "../../../service/publish-manage";
import * as actionType from "./constants"

const changePublishManageListAction=(publishList)=>{
  return {
    type:actionType.CHANGE_PUBLISH_MANAGE_LIST,
    publishList
  }
}

export const getPublishManageListAction=(username,type)=>{
  return dispatch=>{
    requestPublishManageList(username,type).then(res=>{
      // console.log(res);
      dispatch(changePublishManageListAction(res))
    })
  }
}