import { Map } from "immutable";
import * as actionType from "./constants";

const defaultState = Map({
  publishList: [],
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.CHANGE_PUBLISH_MANAGE_LIST:
      return state.set("publishList", action.publishList);
    default:
      return state;
  }
};

export default reducer;
