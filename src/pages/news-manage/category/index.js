import React, {
  memo,
  useState,
  useCallback,
  useEffect,
  useRef,
  useContext,
} from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { DeleteOutlined, ExclamationCircleOutlined } from "@ant-design/icons";
import { Button, Table, Modal, Form, Input, message } from "antd";

import { getNewsCategoryAction } from "../store/action";
import { deleteCategory, updateCategory } from "../../../service/news-manage";

const { confirm } = Modal;

export default memo(function Category() {
  const [category, setCategory] = useState([]);

  const { newsCategory } = useSelector((state) => {
    return {
      newsCategory: state.getIn(["newsManage", "newsCategory"]),
    };
  }, shallowEqual);
  // console.log(category);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getNewsCategoryAction());
  }, [dispatch]);
  useEffect(() => {
    setCategory(newsCategory);
  }, [newsCategory]);

  const handleSave = (record) => {
    // console.log(record)

    setCategory(
      category.map((item) => {
        if (item.id === record.id) {
          return {
            id: item.id,
            title: record.title,
            value: record.title,
          };
        }
        return item;
      })
    );
    updateCategory(record.id, {
      title: record.title,
      value: record.title,
    }).then(res=>{
      message.success("修改成功！")
    })
  };

  const confirmMethod = useCallback(
    (item) => {
      confirm({
        title: "你确定要删除?",
        icon: <ExclamationCircleOutlined />,
        // content: 'Some descriptions',
        okText: "确定",
        cancelText: "取消",
        onOk() {
          //   console.log('OK');
          // deleteMethod(item);
          setCategory(category.filter((data) => data.id !== item.id));
          deleteCategory(item.id).then((res) => {
            message.success("删除成功！");
          });
        },
        onCancel() {
          //   console.log('Cancel');
          message.info("已撤销删除！");
        },
      });
    },
    [category]
  );
  //删除
  // const deleteMethod = (item) => {
  //   // console.log(item)
  //   // 当前页面同步状态 + 后端同步
  //   setCategory(category.filter((data) => data.id !== item.id));
  //   axios.delete(`/categories/${item.id}`);
  // };

  const EditableContext = React.createContext(null);

  const EditableRow = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
      <Form form={form} component={false}>
        <EditableContext.Provider value={form}>
          <tr {...props} />
        </EditableContext.Provider>
      </Form>
    );
  };

  const EditableCell = ({
    title,
    editable,
    children,
    dataIndex,
    record,
    handleSave,
    ...restProps
  }) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef(null);
    const form = useContext(EditableContext);
    useEffect(() => {
      if (editing) {
        inputRef.current.focus();
      }
    }, [editing]);

    const toggleEdit = () => {
      setEditing(!editing);
      form.setFieldsValue({
        [dataIndex]: record[dataIndex],
      });
    };

    const save = async () => {
      try {
        const values = await form.validateFields();
        toggleEdit();
        handleSave({ ...record, ...values });
      } catch (errInfo) {
        console.log("Save failed:", errInfo);
      }
    };

    let childNode = children;

    if (editable) {
      childNode = editing ? (
        <Form.Item
          style={{
            margin: 0,
          }}
          name={dataIndex}
          rules={[
            {
              required: true,
              message: `${title} is required.`,
            },
          ]}
        >
          <Input ref={inputRef} onPressEnter={save} onBlur={save} />
        </Form.Item>
      ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
    }

    return <td {...restProps}>{childNode}</td>;
  };

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      render: (id) => {
        return <b>{id}</b>;
      },
    },
    {
      title: "栏目名称",
      dataIndex: "title",
      onCell: (record) => ({
        record,
        editable: true,
        dataIndex: "title",
        title: "栏目名称",
        handleSave: handleSave,
      }),
    },
    {
      title: "操作",
      render: (item) => {
        return (
          <div>
            <Button
              danger
              shape="circle"
              icon={<DeleteOutlined />}
              onClick={() => confirmMethod(item)}
            />
          </div>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        dataSource={category}
        columns={columns}
        pagination={{
          pageSize: 5,
        }}
        rowKey={(item) => item.id}
        components={{
          body: {
            row: EditableRow,
            cell: EditableCell,
          },
        }}
      />
    </div>
  );
});
