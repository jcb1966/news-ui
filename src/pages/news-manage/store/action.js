import {
  requestNewsCategory,
  requestNewstList,
  requestCurrentNews,
} from "@/service/news-manage";
import * as actionType from "./constants";

/* 内部使用的action */
const changeNewsCategoryAction = (category) => {
  return {
    type: actionType.CHANGE_CATEGORY,
    newsCategory: category,
  };
};

const changeNewsListAction = (newsList) => {
  return {
    type: actionType.CHANGE_NEWS_LIST,
    newsList: newsList,
  };
};

const changeCurrentNewsAction = (currentNews) => {
  return {
    type: actionType.CHANGE_CURRENT_NEWS,
    currentNews: currentNews,
  };
};

/* 向外暴露的action */
export const getNewsCategoryAction = () => {
  return (dispatch) => {
    requestNewsCategory().then((res) => {
      dispatch(changeNewsCategoryAction(res));
    });
  };
};

export const getNewsListAction = (username, auditState) => {
  return (dispatch) => {
    requestNewstList(username, auditState).then((res) => {
      // console.log(res);
      dispatch(changeNewsListAction(res));
    });
  };
};

export const getCurrentNewAction = (id) => {
  return (dispatch) => {
    requestCurrentNews(id).then((res) => {
      // console.log(res);
      dispatch(changeCurrentNewsAction(res));
    });
  };
};
