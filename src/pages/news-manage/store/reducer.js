import { Map } from "immutable";
import * as actionType from "./constants";

const defaultState = Map({
  newsCategory: [],
  newsList: [],
  currentNews: null,
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.CHANGE_CATEGORY:
      return state.set("newsCategory", action.newsCategory);
    case actionType.CHANGE_NEWS_LIST:
      return state.set("newsList", action.newsList);
    case actionType.CHANGE_CURRENT_NEWS:
      return state.set("currentNews", action.currentNews);
    default:
      return state;
  }
};

export default reducer;
