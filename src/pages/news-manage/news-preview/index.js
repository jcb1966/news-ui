import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import moment from "moment";
import { PageHeader, Descriptions, Tag } from "antd";

import { getCurrentNewAction } from "../store/action";
import { NewsPreviewWrapper } from "./style";

export default memo(function NewsPreview(props) {
  const [currentNewState, setCurrentNewState] = useState(null);

  const { currentNews } = useSelector((state) => {
    return {
      currentNews: state.getIn(["newsManage", "currentNews"]),
    };
  }, shallowEqual);
  // console.log(props);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCurrentNewAction(props.match.params.id));
  }, [dispatch, props.match.params.id]);
  useEffect(() => {
    setCurrentNewState(currentNews);
  }, [currentNews]);
  // console.log(currentNewState);
  const auditList = ["未审核", "审核中", "已通过", "未通过"];
  const publishList = ["未发布", "待发布", "已上线", "已下线"];
  return (
    <NewsPreviewWrapper>
      {currentNewState && (
        <div>
          <PageHeader
            onBack={() => window.history.back()}
            title={currentNewState.title}
            subTitle={currentNewState.category.title}
          >
            <Descriptions size="small">
              <Descriptions.Item label="创建者">
                {currentNewState.author}
              </Descriptions.Item>
              <Descriptions.Item label="创建时间">
                {moment(currentNewState.createTime).format(
                  "YYYY/MM/DD HH:mm:ss"
                )}
              </Descriptions.Item>
              <Descriptions.Item label="发布时间">
                {currentNewState.publishTime
                  ? moment(currentNewState.publishTime).format(
                      "YYYY/MM/DD HH:mm:ss"
                    )
                  : "-"}
              </Descriptions.Item>
              <Descriptions.Item label="区域">
                {currentNewState.region}
              </Descriptions.Item>
              <Descriptions.Item label="审核状态" className="status">
                <Tag color="red">{auditList[currentNewState.auditState]}</Tag>
              </Descriptions.Item>
              <Descriptions.Item label="发布状态">
                <Tag color="red">
                  {publishList[currentNewState.publishState]}
                </Tag>
              </Descriptions.Item>
              <Descriptions.Item label="访问数量">
                <Tag color="green">{currentNewState.view}</Tag>
              </Descriptions.Item>
              <Descriptions.Item label="点赞数量">
                <Tag color="green">{currentNewState.star}</Tag>
              </Descriptions.Item>
              <Descriptions.Item label="评论数量">
                <Tag color="green">0</Tag>
              </Descriptions.Item>
            </Descriptions>
          </PageHeader>

          <div
            dangerouslySetInnerHTML={{
              __html: currentNewState.content,
            }}
            style={{
              margin: "0 24px",
              border: "1px solid gray",
            }}
          ></div>
        </div>
      )}
    </NewsPreviewWrapper>
  );
});
