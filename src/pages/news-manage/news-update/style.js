import styled from "styled-components";

export const AddNewsWrapper = styled.div`
  .content {
    height: 300px;
    .activeStep {
      margin-top: 20px;
      .editorClassName {
        height: 250px !important;
        border: 1px solid #f1f1f1 !important;
        padding: 5px !important;
        border-radius: 2px !important;
      }
      .ant-btn{
        margin-right:10px;
      }
    }
    .hiddenStep {
      display: none;
      margin-top: 20px;
    }
  }
  .steps{
    margin-top:20px;
    position:relative;
    .step-btn{
      position:absolute;
      right:10px;
      .ant-btn{
        margin-left:10px;
      }
    }
  }
`;
