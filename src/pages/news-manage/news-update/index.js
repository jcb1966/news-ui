import React, { memo, useState, useCallback, useEffect, useRef } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Steps, Button, Form, Input, Select, message, PageHeader } from "antd";
import { RightCircleOutlined, LeftCircleOutlined } from "@ant-design/icons";

import { AddNewsWrapper } from "./style";
import NewsEditor from "@/components/news-editor/NewsEditor";
import { getNewsCategoryAction, getCurrentNewAction } from "../store/action";
import { updateNews } from "@/service/news-manage";

const { Step } = Steps;
const { Option } = Select;
// const { TextArea } = Input;

export default memo(function NewsUpdate(props) {
  const [currentStep, setCurrnetStep] = useState(0);
  const [category, setCategory] = useState([]);
  const [content, setContent] = useState("");
  const [baseInfo, setBaseInfo] = useState(null);
  const [updateNewsInfo, setUpdateNewsInfo] = useState(null);

  const baseMsgFormRef = useRef();
  const { newsCategory, currentNews } = useSelector((state) => {
    return {
      newsCategory: state.getIn(["newsManage", "newsCategory"]),
      currentNews: state.getIn(["newsManage", "currentNews"]),
    };
  }, shallowEqual);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getNewsCategoryAction());
    dispatch(getCurrentNewAction(props.match.params.id));
  }, [dispatch, props.match.params.id]);
  useEffect(() => {
    setCategory(newsCategory);
  }, [newsCategory]);
  useEffect(() => {
    setUpdateNewsInfo(currentNews);
  }, [currentNews]);
  // console.log(updateNewsInfo);
  useEffect(() => {
    baseMsgFormRef.current.setFieldsValue({
      title: updateNewsInfo && updateNewsInfo.title,
      categoryId: updateNewsInfo && updateNewsInfo.categoryId,
    });
  }, [updateNewsInfo]);

  const nestStep = useCallback(() => {
    if (currentStep === 0) {
      baseMsgFormRef.current
        .validateFields()
        .then((value) => {
          // console.log(value);
          setBaseInfo(value);
          setCurrnetStep(currentStep + 1);
        })
        .catch((err) => {});
    } else {
      // console.log(content);
      if (content === "" || content.trim() === "<p></p>") {
        message.error("新闻内容不能为空");
      } else {
        setCurrnetStep(currentStep + 1);
      }
    }
  }, [currentStep, content]);

  const preStep = useCallback(() => {
    setCurrnetStep(currentStep - 1);
  }, [currentStep]);

  const submitNews = useCallback(
    (auditState) => {
      // const User = JSON.parse(localStorage.getItem("token"));
      updateNews(props.match.params.id, {
        ...baseInfo,
        content: content,
        auditState: auditState,
      }).then((res) => {
        // console.log(res);
        props.history.push(
          auditState === 0 ? "/news-manage/draft" : "/audit-manage/list"
        );
        message.success(`${auditState === 0 ? "保存成功" : "提交成功"}`);
      });
    },
    [baseInfo, content, props.history, props.match.params.id]
  );

  return (
    <AddNewsWrapper>
      <PageHeader onBack={() => window.history.back()} title="更新新闻">
        <Steps current={currentStep} size="small">
          <Step title="基本信息" description="新闻标题，新闻分类" />
          <Step title="新闻内容" description="新闻主体内容" />
          <Step title="新闻提交" description="保存草稿或者提交审核" />
        </Steps>

        <div className="content">
          <div className={currentStep === 0 ? "activeStep" : "hiddenStep"}>
            <Form
              name="basic"
              labelCol={{ span: 2 }}
              wrapperCol={{ span: 22 }}
              autoComplete="off"
              ref={baseMsgFormRef}
            >
              <Form.Item
                label="新闻标题"
                name="title"
                rules={[{ required: true, message: "请输入新闻标题" }]}
              >
                <Input placeholder="请输入新闻标题" />
              </Form.Item>
              <Form.Item
                label="新闻分类"
                name="categoryId"
                rules={[{ required: true, message: "请选择新闻分类" }]}
              >
                <Select placeholder="请选择新闻分类">
                  {category.map((item) => {
                    return (
                      <Option value={item.id} key={item.id}>
                        {item.title}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Form>
          </div>
          <div className={currentStep === 1 ? "activeStep" : "hiddenStep"}>
            <NewsEditor
              getContent={(value) => setContent(value)}
              content={updateNewsInfo && updateNewsInfo.content}
            ></NewsEditor>
            {/* <TextArea rows={4} placeholder="请输入新闻内容"  /> */}
          </div>
          <div className={currentStep === 2 ? "activeStep" : "hiddenStep"}>
            <Button type="primary" onClick={(e) => submitNews(0)}>
              保存至草稿箱
            </Button>
            <Button type="primary" danger onClick={(e) => submitNews(1)}>
              提交审核
            </Button>
          </div>
        </div>

        <div className="steps">
          <div className="step-btn">
            {currentStep > 0 && (
              <Button onClick={(e) => preStep()} icon={<LeftCircleOutlined />}>
                上一步
              </Button>
            )}
            {currentStep < 2 && (
              <Button
                type="primary"
                onClick={(e) => nestStep()}
                icon={<RightCircleOutlined />}
              >
                下一步
              </Button>
            )}
          </div>
        </div>
      </PageHeader>
    </AddNewsWrapper>
  );
});
