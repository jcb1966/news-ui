import React, { memo, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Table, Button, Modal, message } from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  UpCircleOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { getNewsListAction } from "../store/action";
import { requestDeleteNews, updateNews } from "../../../service/news-manage";
import { DraftWrapper } from "./style";

const { confirm } = Modal;

export default memo(function Draft(props) {
  const [draftLIst, setDraftLIst] = useState([]);

  const { newsList } = useSelector((state) => {
    return {
      newsList: state.getIn(["newsManage", "newsList"]),
    };
  }, shallowEqual);

  const userInfo = JSON.parse(localStorage.getItem("token"));

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getNewsListAction(userInfo.username, 0));
  }, [dispatch, userInfo.username]);
  useEffect(() => {
    setDraftLIst(newsList);
  }, [newsList]);
  // console.log(draftLIst);

  const deleteNews = useCallback(
    (row) => {
      confirm({
        title: "警告",
        icon: <ExclamationCircleOutlined />,
        content: "你确定要删除这一项吗？",
        okText: "确认",
        cancelText: "取消",
        onOk() {
          requestDeleteNews(row.id).then((res) => {
            setDraftLIst(draftLIst.filter((item) => item.id !== row.id));
            message.success("删除成功！");
          });
        },
        onCancel() {
          message.info("已撤销删除！");
        },
      });
    },
    [draftLIst]
  );
  const submitNews = useCallback(
    (row) => {
      updateNews(row.id, {
        auditState: 1,
      }).then((res) => {
        // console.log(res);
        props.history.push("/audit-manage/list");
        message.success("提交成功");
      });
    },
    [props.history]
  );

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      render: (id) => {
        return <b>{id}</b>;
      },
    },
    {
      title: "标题",
      dataIndex: "title",
      render(col, row) {
        return <a href={`#/news-manage/preview/${row.id}`}>{col}</a>;
      },
    },
    {
      title: "作者",
      dataIndex: "author",
    },
    {
      title: "分类",
      dataIndex: "category",
      render(col) {
        return col.title;
      },
    },
    {
      title: "操作",
      render(row) {
        return (
          <div>
            <Button
              danger
              className="button"
              type="primary"
              shape="circle"
              icon={<DeleteOutlined />}
              onClick={() => deleteNews(row)}
            />
            <Button
              className="button"
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => {
                props.history.push(`/news-manage/update/${row.id}`);
              }}
            />
            <Button
              className="button"
              type="primary"
              shape="circle"
              icon={<UpCircleOutlined />}
              onClick={() => submitNews(row)}
            />
          </div>
        );
      },
    },
  ];
  return (
    <DraftWrapper>
      <Table
        dataSource={draftLIst}
        columns={columns}
        rowKey={(row) => row.id}
      />
    </DraftWrapper>
  );
});
