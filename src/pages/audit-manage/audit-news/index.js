import React, { memo, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Table, Button, message } from "antd";

import { getAuditListOfUnpublishAction } from "../store/action";
import { updateNews } from "@/service/news-manage";

export default memo(function AuditNews() {
  const [auditList, setAuditList] = useState([]);

  const { auditListOfUnpublish } = useSelector((state) => {
    return {
      auditListOfUnpublish: state.getIn([
        "auditManage",
        "auditListOfUnpublish",
      ]),
    };
  }, shallowEqual);
  // console.log(auditListOfUnpublish);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAuditListOfUnpublishAction());
  }, [dispatch]);
  useEffect(() => {
    const { roleId, region, username } = JSON.parse(
      localStorage.getItem("token")
    );
    const roleObj = {
      1: "superadmin",
      2: "admin",
      3: "editor",
    };
    setAuditList(
      roleObj[roleId] === "superadmin"
        ? auditListOfUnpublish
        : [
            ...auditListOfUnpublish.filter((item) => item.author === username),
            ...auditListOfUnpublish.filter(
              (item) =>
                item.region === region && roleObj[item.roleId] === "editor"
            ),
          ]
    );
  }, [auditListOfUnpublish]);

  const examineNews = useCallback(
    (row, auditState, publishState) => {
      setAuditList(auditList.filter((item) => item.id !== row.id));
      updateNews(row.id, { auditState, publishState }).then((res) => {
        message.success("审核已提交！");
      });
    },
    [auditList]
  );

  const columns = [
    {
      title: "新闻标题",
      dataIndex: "title",
      render: (title, item) => {
        return <a href={`#/news-manage/preview/${item.id}`}>{title}</a>;
      },
    },
    {
      title: "作者",
      dataIndex: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "category",
      render: (category) => {
        return <div>{category.title}</div>;
      },
    },
    {
      title: "操作",
      render: (item) => {
        return (
          <div>
            <Button type="primary" onClick={() => examineNews(item, 2, 1)}>
              通过
            </Button>
            <Button danger onClick={() => examineNews(item, 3, 0)}>
              驳回
            </Button>
          </div>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        dataSource={auditList}
        columns={columns}
        pagination={{
          pageSize: 5,
        }}
        rowKey={(item) => item.id}
      />
    </div>
  );
});
