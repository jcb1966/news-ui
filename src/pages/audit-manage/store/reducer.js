import { Map } from "immutable";
import * as actionType from "./constants";

const defaultState = Map({
  auditList: [],
  auditListOfUnpublish: [],
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.CHANGE_AUDIT_LIST:
      return state.set("auditList", action.auditList);
    case actionType.CHANGE_AUDIT_LIST_OF_UNPUBLISH:
      return state.set("auditListOfUnpublish", action.auditListOfUnpublish);
    default:
      return state;
  }
};

export default reducer;
