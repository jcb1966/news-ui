import {
  requestAuditList,
  requestAuditListOfUnpublish,
} from "@/service/audit-manage";
import * as actionType from "./constants";

const changeAuditListAction = (auditList) => {
  return {
    type: actionType.CHANGE_AUDIT_LIST,
    auditList,
  };
};

const changeAuditListOfUnpublishAction = (auditListOfUnpublish) => {
  return {
    type: actionType.CHANGE_AUDIT_LIST_OF_UNPUBLISH,
    auditListOfUnpublish,
  };
};

/* 向外暴露的action */
export const getAuditListAction = (username) => {
  return (dispatch) => {
    requestAuditList(username).then((res) => {
      // console.log(res);
      dispatch(changeAuditListAction(res));
    });
  };
};

export const getAuditListOfUnpublishAction = () => {
  return (dispatch) => {
    requestAuditListOfUnpublish().then((res) => {
      // console.log(res);
      dispatch(changeAuditListOfUnpublishAction(res));
    });
  };
};
