import React, { memo, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { Table, Button, Tag,message } from "antd";
import {
  StopOutlined,
  RedoOutlined,
  VerticalAlignTopOutlined,
} from "@ant-design/icons";

import { getAuditListAction } from "../store/action";
import { updateNews } from "@/service/news-manage";

export default memo(function AuditList(props) {
  const [auditListState, setAuditListState] = useState([]);

  const { auditList } = useSelector((state) => {
    return {
      auditList: state.getIn(["auditManage", "auditList"]),
    };
  }, shallowEqual);
  // console.log(auditListState);

  const dispatch = useDispatch();

  useEffect(() => {
    const userInfo = JSON.parse(localStorage.getItem("token"));
    dispatch(getAuditListAction(userInfo.username));
  }, [dispatch]);
  useEffect(() => {
    setAuditListState(auditList);
  }, [auditList]);

  const cancelNews = useCallback(
    (row) => {
      console.log(row);
      updateNews(row.id, { auditState: 0 }).then((res) => {
        setAuditListState(auditListState.filter((item) => item.id !== row.id));
        message.success('已撤销！');
      });
    },
    [auditListState]
  );
  const updateBtnClick = useCallback(
    (row) => {
      props.history.push(`/news-manage/update/${row.id}`);
    },
    [props.history]
  );
  const publishNews = useCallback((row) => {
    updateNews(row.id, {
      publishState: 2,
      publishTime: Date.now(),
    }).then(res=>{
      props.history.push('/publish-manage/published')
      message.success("已发布！")
    })
  },[props.history]);

  const columns = [
    {
      title: "新闻标题",
      dataIndex: "title",
      render: (title, item) => {
        return <a href={`#/news-manage/preview/${item.id}`}>{title}</a>;
      },
    },
    {
      title: "作者",
      dataIndex: "author",
    },
    {
      title: "新闻分类",
      dataIndex: "category",
      render: (category) => {
        return <div>{category.title}</div>;
      },
    },
    {
      title: "审核状态",
      dataIndex: "auditState",
      render: (auditState) => {
        const colorList = ["", "orange", "green", "red"];
        const auditList = ["草稿箱", "审核中", "已通过", "未通过"];
        return <Tag color={colorList[auditState]}>{auditList[auditState]}</Tag>;
      },
    },
    {
      title: "操作",
      render: (item) => {
        return (
          <div>
            {item.auditState === 1 && (
              <Button
                type="link"
                icon={<StopOutlined />}
                onClick={() => cancelNews(item)}
              >
                撤销
              </Button>
            )}
            {item.auditState === 2 && (
              <Button
                type="link"
                icon={<VerticalAlignTopOutlined />}
                onClick={() => publishNews(item)}
              >
                发布
              </Button>
            )}
            {item.auditState === 3 && (
              <Button
                type="link"
                icon={<RedoOutlined />}
                onClick={() => {
                  updateBtnClick(item);
                }}
              >
                更新
              </Button>
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div>
      <Table
        dataSource={auditListState}
        columns={columns}
        rowKey={(row) => row.id}
      />
    </div>
  );
});
