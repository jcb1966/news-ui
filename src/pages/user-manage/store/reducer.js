import { Map } from "immutable";
import * as actionType from "./constants";

const defaultState = Map({
  userList: [],
  regionList:[]
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionType.CHANGE_USER_LIST:
      return state.set("userList", action.userList);
    case actionType.CHANGE_REGION_LIST:
      return state.set("regionList",action.regionList)
    default:
      return state;
  }
};

export default reducer
