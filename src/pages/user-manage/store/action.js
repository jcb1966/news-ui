import { requestUserList,requestRegionList } from "@/service/user-manage"
import * as actionType from "./constants"

/* 内部自己使用的action */
const changeUserListAction=(res)=>{
  return {
    type:actionType.CHANGE_USER_LIST,
    userList:res
  }
}

const changeRegionListAction=(res)=>{
  return {
    type:actionType.CHANGE_REGION_LIST,
    regionList:res
  }
}

/* 向外暴露的action */
export const getUserListAction=()=>{
  return dispatch=>{
    requestUserList().then(res=>{
      // console.log(res);
      dispatch(changeUserListAction(res))
    })
  }
}

export const getRegionListAction=()=>{
  return dispatch=>{
    requestRegionList().then(res=>{
      // console.log(res);
      dispatch(changeRegionListAction(res))
    })
  }
}