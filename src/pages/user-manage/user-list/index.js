import React, { memo, useState, useEffect, useCallback, useRef } from "react";
import { useDispatch, shallowEqual, useSelector } from "react-redux";

import { Table, Switch, Button, Modal, message } from "antd";
import {
  DeleteOutlined,
  AlignCenterOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";

import { getUserListAction, getRegionListAction } from "../store/action";
import { getRoleListAction } from "../../right-manage/store/action";
import {
  requestAddUser,
  requestDeleteUser,
  requestUpdateUserState,
  requestUpdateUser,
} from "@/service/user-manage";

import UserManageForm from "@/components/user-manage-form";

const { confirm } = Modal;

export default memo(function UserManageList() {
  /* state */
  const [userListState, setUserListState] = useState([]); //保存用户列表的数据
  const [isModalVisible, setIsModalVisible] = useState(false); //控制对话框的打开/关闭
  const [modalTitle, setModalTitle] = useState(""); //对话框名称
  const [regionFormItemIdDisabled, setRegionFormItemIdDisabled] =
    useState(false); //表单组件的区域选择框是否可用
  const [currentUser, setCurrentUser] = useState(null);

  const userInfo = JSON.parse(localStorage.getItem("token"));

  /* redux */
  const { userList, regionList, roleList } = useSelector((state) => {
    return {
      userList: state.getIn(["userManage", "userList"]), //用户列表的数据
      regionList: state.getIn(["userManage", "regionList"]), //区域列表
      roleList: state.getIn(["rightManage", "roleList"]), //角色列表
    };
  }, shallowEqual);
  const dispatch = useDispatch();

  /* hook */
  useEffect(() => {
    dispatch(getUserListAction());
    dispatch(getRegionListAction());
    dispatch(getRoleListAction());
  }, [dispatch]);
  useEffect(() => {
    const formatUserList =
      userInfo.roleId === 1
        ? userList
        : [
            ...userList.filter((item) => item.id === userInfo.id),
            ...userList.filter(
              (item) => item.region === userInfo.region && item.roleId === 3
            ),
          ];
    setUserListState(formatUserList);
  }, [userList, userInfo.roleId, userInfo.id, userInfo.region]);
  const addUserForm = useRef();
  // console.log("userList",userList);
  // console.log("roleList",roleList);
  // console.log(regionList);

  /* 事件调用 */
  // 添加用户
  const addUserBtnClick = useCallback(() => {
    setModalTitle("添加用户");
    setIsModalVisible(true);
    setRegionFormItemIdDisabled(false);
    setCurrentUser(null);
  }, []);

  // 监听确定按钮的点击
  const handleOk = useCallback(() => {
    if (modalTitle === "添加用户") {
      // 触发表单的验证
      addUserForm.current
        .validateFields()
        .then((value) => {
          /* 通过验证 */
          requestAddUser({
            ...value,
            roleState: true,
            default: false,
          }).then((res) => {
            // json-server返回的数据中没有role字段，这是因为jsonserver不会在数据请求之后自动连接数据库
            // 所以需要手动添加role字段才能避免出现 角色名称 为空的bug
            // setUserListState([...userListState,res]);
            setUserListState([
              ...userListState,
              {
                ...res,
                role: roleList.filter((item) => item.id === res.roleId)[0],
              },
            ]);
            // 关闭对话框
            setIsModalVisible(false);
            // 重置表单
            addUserForm.current.resetFields();
            message.success("添加用户成功！", 1);
          });
        })
        .catch((err) => {
          /* 未通过验证 */
        });
    } else {
      addUserForm.current.validateFields().then((form) => {
        requestUpdateUser(currentUser, form).then((res) => {
          setUserListState(
            userListState.map((item) => {
              if (item.id === res.id) {
                return {
                  ...res,
                  role: roleList.filter(
                    (roleItem) => roleItem.id === res.roleId
                  )[0],
                };
              }
              return item;
            })
          );
          setIsModalVisible(false);
          addUserForm.current.resetFields();
          message.success("更新用户成功！", 1);
        });
      });
    }
  }, [userListState, roleList, modalTitle, currentUser]);

  const handleCancel = useCallback(() => {
    setIsModalVisible(false);
    addUserForm.current.resetFields();
  }, []);

  // 删除用户
  const deleteUser = useCallback(
    (item) => {
      confirm({
        title: "你确定要删除此用户吗?",
        icon: <ExclamationCircleOutlined />,
        okText: "确定",
        okType: "danger",
        cancelText: "返回",
        onOk() {
          setUserListState(userListState.filter((data) => data.id !== item.id));
          requestDeleteUser(item.id);
          message.success("删除成功！", 1);
        },
        onCancel() {
          message.success("已撤销删除！", 1);
        },
      });
    },
    [userListState]
  );
  // 修改用户的状态
  const updateUserState = useCallback(
    (item) => {
      item.roleState = !item.roleState;
      setUserListState([...userListState]);
      requestUpdateUserState(item.id, item.roleState);
      message.success("修改用户状态成功！", 1);
    },
    [userListState]
  );
  // 修改用户权限
  const updateUserRight = useCallback((row) => {
    setModalTitle("更新用户");
    row.role.id === 1 && setRegionFormItemIdDisabled(true);
    row.role.id !== 1 && setRegionFormItemIdDisabled(false);
    setCurrentUser(row.id);
    // react中状态的更新并不一定是同步的
    setTimeout(() => {
      setIsModalVisible(true);
      addUserForm.current.setFieldsValue(row);
    });
  }, []);

  /* 其他业务逻辑 */
  // 表格的数据结构
  const columns = [
    {
      title: "区域",
      dataIndex: "region",
      filters: [
        {
          text: "全球",
          value: "全球",
        },
        ...regionList.map((item) => ({
          text: item.title,
          value: item.value,
        })),
      ],
      onFilter(value, row) {
        if (value === "全球") return row.region === "";
        return value === row.region;
      },
      render(region) {
        return <b>{region === "" ? "全球" : region}</b>;
      },
    },
    {
      title: "角色名称",
      dataIndex: "role",
      render(role) {
        return role.roleName;
      },
    },
    {
      title: "用户名",
      dataIndex: "username",
    },
    {
      title: "用户状态",
      dataIndex: "roleState",
      render(roleState, item) {
        return (
          <Switch
            checked={roleState}
            disabled={item.id === userInfo.id || item.default}
            onChange={(e) => updateUserState(item)}
          />
        );
      },
    },
    {
      title: "操作",
      render(item) {
        return (
          <>
            <Button
              danger
              type="danger"
              shape="circle"
              icon={<DeleteOutlined />}
              className="button-icon-marging"
              disabled={item.id === userInfo.id || item.default}
              onClick={(e) => deleteUser(item)}
            />
            <Button
              type="primary"
              shape="circle"
              icon={<AlignCenterOutlined />}
              disabled={item.id === userInfo.id || item.default}
              onClick={(e) => updateUserRight(item)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div>
      <Button
        type="primary"
        className="top-button"
        onClick={(e) => {
          addUserBtnClick();
        }}
      >
        添加用户
      </Button>
      <Table
        rowKey={(item) => item.id}
        dataSource={userListState}
        columns={columns}
        pagination={{ pageSize: 5 }}
      />

      {/* 添加/修改用户的对话框 */}
      <Modal
        title={modalTitle}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText="取消"
        okText="确定"
      >
        <UserManageForm
          ref={addUserForm}
          regionList={regionList}
          roleList={roleList}
          regionFormItemIdDisabled={regionFormItemIdDisabled}
        ></UserManageForm>
      </Modal>
    </div>
  );
});
